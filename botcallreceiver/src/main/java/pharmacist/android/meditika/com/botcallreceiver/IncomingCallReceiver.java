package pharmacist.android.meditika.com.botcallreceiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by Irfan on 28/05/16.
 */
public class IncomingCallReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(final Context context, Intent intent) {
        TelephonyManager telephony = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
        telephony.listen(new PhoneStateListener(){
            @Override
            public void onCallStateChanged(int state, String incomingNumber) {
                super.onCallStateChanged(state, incomingNumber);

                Log.d("Bot","incomingNumber : "+incomingNumber);
                Toast.makeText(context,incomingNumber,Toast.LENGTH_LONG).show();
            }
        }, PhoneStateListener.LISTEN_CALL_STATE);
    }
}