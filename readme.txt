
<ol>
<li>Download icons from here: Download in png format with size of 96px. Put icon into drawable-xxxhdpi folder.
https://icons8.com/web-app/for/androidL/
</li>

<li>
Generate launcher icon from here:
https://makeappicon.com/
</li>

<li>
Dependency injection using butterknife- http://jakewharton.github.io/butterknife/
</li>

<li></li>