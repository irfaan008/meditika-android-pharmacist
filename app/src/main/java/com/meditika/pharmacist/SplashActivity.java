package com.meditika.pharmacist;

import android.content.Intent;
import android.os.Bundle;

import com.meditika.pharmacist.auth.LoginActivity;
import com.meditika.pharmacist.share.BaseActivity;
import com.meditika.pharmacist.share.MainActivity;
import com.meditika.pharmacist.util.Const;
import com.meditika.pharmacist.util.storage.Pref;

/**
 * Created by Irfan on 23/05/16.
 */
public class SplashActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        final Intent intent;

        String authToken= Pref.Read(getApplicationContext(), Const.KEY_AUTH_TOKEN);

        if(authToken.isEmpty())
            intent=new Intent(this,LoginActivity.class);
        else intent=new Intent(this,MainActivity.class);

        Thread background = new Thread() {
            public void run() {

                try {
                    sleep(2000 * 1);
                    startActivity(intent);
                    finish();

                } catch (Exception e) {

                }
            }
        };
        background.start();
    }
}
