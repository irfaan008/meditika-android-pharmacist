package com.meditika.pharmacist.auth;

import com.google.gson.annotations.SerializedName;

/**
 * Created by admin on 6/2/2016.
 */
public class PojoParticularCity {




    @SerializedName("city_id")
    protected String city_id;

    public void setCityId(String stateName) {
        this.city_id = stateName;
    }

    public String getCityId() {
        return city_id;
    }

    @SerializedName("city_name")
    protected String city_name;

    public void setCityName(String stateName) {
        this.city_name = stateName;
    }

    public String getCityName() {
        return city_name;
    }


    @SerializedName("state_id")
    protected String state_id;
    public String getStateId() {
        return state_id;
    }

    public void setStateId(String stateId) {
        this.state_id = stateId;
    }


    @SerializedName("state_name")
    protected String state_name;

    public void setStateName(String stateName) {
        this.state_name = stateName;
    }

    public String getStateName() {
        return state_name;
    }



}
