package com.meditika.pharmacist.auth;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.ArrayList;

/**
 * Created by Irfan on 09/01/16.
 */
@Parcel
public class PojoUser {

    @SerializedName("status")
    protected String status;

    @SerializedName("message")
    protected String message;

    @SerializedName("code")
    protected int code;

    @SerializedName("data")
    protected ArrayList<BoUser> allUser;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }


    public ArrayList<BoUser> getAllUser() {
        return allUser;
    }

    public void setAllUser(ArrayList<BoUser> allUser) {
        this.allUser = allUser;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;

    }
}
