package com.meditika.pharmacist.auth;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.meditika.pharmacist.R;

import com.meditika.pharmacist.share.BaseActivity;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Vishwa android on 31-May-16.
 */
public class RegistrationActivity extends BaseActivity {

    static int fragmentCount = 0;
    @Bind(R.id.imgnext)
    ImageView nextButton;

    @Bind(R.id.imgpreviuos)
    ImageView previousButton;

    @Bind(R.id.toolbar_registraion)
    Toolbar toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registeration);
        ButterKnife.bind(this);

        toolbar = (Toolbar) findViewById(R.id.toolbar_registraion);
        setSupportActionBar(toolbar);


        callFragment(0);
        if (fragmentCount == 0)
            previousButton.setVisibility(View.GONE);
    }

    @OnClick(R.id.imgpreviuos)
    public void previousFragment() {
        if (fragmentCount != 0) {
            fragmentCount--;
            if (fragmentCount == 0)
                previousButton.setVisibility(View.GONE);
            if (!nextButton.isShown())
                nextButton.setVisibility(View.VISIBLE);
            callFragment(fragmentCount);
        }


    }

    @OnClick(R.id.imgnext)
    public void nextFragment() {
        if (fragmentCount != 3) {
            fragmentCount++;
            if (fragmentCount == 3)
                nextButton.setVisibility(View.INVISIBLE);
            if (!previousButton.isShown())
                previousButton.setVisibility(View.VISIBLE);

            callFragment(fragmentCount);

        }

    }

    private void callFragment(int fragmentCount) {

        Fragment fragment = null;
        switch (fragmentCount) {

            case 0:
                fragment = new Fragment1_Registration();
                toolbar.setTitle("    Location Detail");
                break;
            case 1:
                fragment = new Fragment2_Registration();
                toolbar.setTitle("    Pharmacy detail");
                break;

            case 2:
                fragment = new Fragment3_Registration();
                toolbar.setTitle("    License detail");
                break;

            case 3:
                fragment = new Fragment4_Registration();
                toolbar.setTitle("    Services");
                break;

        }

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frame, fragment).addToBackStack(null).commit();


    }


}
