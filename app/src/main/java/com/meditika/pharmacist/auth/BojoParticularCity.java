package com.meditika.pharmacist.auth;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by admin on 6/2/2016.
 */
public class BojoParticularCity {
    @SerializedName("status")
    protected String status;

    public void setStatus(String stateName) {
        this.status = stateName;
    }

    public String getStatus() {
        return status;
    }


    @SerializedName("code")
    protected String code;

    public void setCode(String stateName) {
        this.code = stateName;
    }

    public String getCode() {
        return code;
    }


    @SerializedName("records")
    protected String records;

    public void setRecords(String stateName) {
        this.records = stateName;
    }

    public String getRecords() {
        return records;
    }


    @SerializedName("data")
    protected List<PojoParticularCity> data;

    public void setData(List<PojoParticularCity> stateName) {
        this.data = stateName;
    }

    public List<PojoParticularCity> getData() {
        return data;
    }

}
