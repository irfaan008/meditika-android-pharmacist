package com.meditika.pharmacist.auth;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;


/**
 * Created by Irfan on 09/01/16.
 */

@Parcel
public class BoUser {

    @SerializedName("account_id")
    protected String accountId;

    @SerializedName("display_name")
    protected String displayName;

    @SerializedName("pharmacist_store_name")
    protected String storeName;

    @SerializedName("display_address")
    protected String address;

    @SerializedName("pharmacist_registered_name")
    protected String pharmacistName;

    @SerializedName("username")
    protected String username;

    @SerializedName("auth_token")
    protected String authToken;

    @SerializedName("last_login")
    protected String lastLogin;

    @SerializedName("user_type_name")
    protected String userType;

}
