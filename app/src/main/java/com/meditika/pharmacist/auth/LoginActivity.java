package com.meditika.pharmacist.auth;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.meditika.pharmacist.R;
import com.meditika.pharmacist.share.BaseActivity;
import com.meditika.pharmacist.share.MainActivity;
import com.meditika.pharmacist.util.Common;
import com.meditika.pharmacist.util.Const;
import com.meditika.pharmacist.util.api.APIHelper;
import com.meditika.pharmacist.util.api.IMeditikaAPIService;
import com.meditika.pharmacist.util.storage.Pref;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by admin on 5/24/2016.
 */
public class LoginActivity extends BaseActivity {

    @Bind(R.id.tvVersion)
    TextView tvVersion;

    @Bind(R.id.etUserName)
    EditText etUserName;

    @Bind(R.id.etPassword)
    EditText etPassword;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        ButterKnife.bind(this);

        String versionName= Common.getVersionName(getApplicationContext());
        tvVersion.append(versionName);
    }

    @OnClick(R.id.btnLogin)
    public void onClick(View view){
        String userName=etUserName.getText().toString().trim().toLowerCase();
        String password=etPassword.getText().toString().trim();
        if(userName.length()<1) {
            etUserName.setError("Invalid username");
            return;
        }
        if(password.length()<1) {
            etPassword.setError("Invalid password");
            return;
        }

        performLogin(userName,password);
    }

    private void performLogin(final String userName, String password) {

        IMeditikaAPIService service= APIHelper.getAppServiceMethod();

        Call<PojoUser> call = service.login(userName,password);
        call.enqueue(new Callback<PojoUser>() {
            @Override
            public void onResponse(Call<PojoUser> call, Response<PojoUser> response) {
                // response.isSuccessful() is true if the response code is 2xx
                if (response.isSuccessful()) {
                    PojoUser pojoUser = response.body();
                    Toast.makeText(getApplicationContext(),pojoUser.getStatus(),Toast.LENGTH_LONG).show();
                    if(pojoUser.getCode()==Const.CODE_SUCCESS) {
                        BoUser user = pojoUser.getAllUser().get(0);
                        saveUserDetail(user);
                        redirect();
                    } else if(pojoUser.getCode()==Const.CODE_FAILED){

                    }

                } else {
                    int statusCode = response.code();
                    Toast.makeText(getApplicationContext(),"error: "+statusCode,Toast.LENGTH_LONG).show();
                    // handle request errors yourself
                    ResponseBody errorBody = response.errorBody();
                    log("failed get users : "+errorBody);
                }
            }

            @Override
            public void onFailure(Call<PojoUser> call, Throwable t) {
                // handle execution failures like no internet connectivity
                log("Server error: "+t.getMessage());
                Toast.makeText(getApplicationContext(),"server error",Toast.LENGTH_LONG).show();
            }
        });
    }

    private void redirect() {
        Intent intent=new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    private void saveUserDetail(BoUser user) {
        Pref.Write(getApplicationContext(),Const.KEY_ACCOUNT_ID,user.accountId);
        Pref.Write(getApplicationContext(),Const.KEY_ADDRESS,user.address);
        Pref.Write(getApplicationContext(),Const.KEY_AUTH_TOKEN,user.authToken);
        Pref.Write(getApplicationContext(),Const.KEY_DISPLAY_NAME,user.displayName);
        Pref.Write(getApplicationContext(),Const.KEY_PHARMACIST_NAME,user.pharmacistName);
        Pref.Write(getApplicationContext(),Const.KEY_STORE_NAME,user.storeName);
        Pref.Write(getApplicationContext(),Const.KEY_USER_NAME,user.username);


    }

    private void performLogin1(final String userName, String password) {

        IMeditikaAPIService service= APIHelper.getAppServiceMethod();

        Call<PojoUser> call = service.getUsers();
        call.enqueue(new Callback<PojoUser>() {
            @Override
            public void onResponse(Call<PojoUser> call, Response<PojoUser> response) {
                // response.isSuccessful() is true if the response code is 2xx
                if (response.isSuccessful()) {
                    PojoUser pojoUser = response.body();
                    if(pojoUser.getCode()==Const.CODE_SUCCESS) {
                        BoUser user = pojoUser.getAllUser().get(0);
                        Toast.makeText(getApplicationContext(),user.displayName,Toast.LENGTH_LONG).show();
                    }

                } else {
                    int statusCode = response.code();
                    Toast.makeText(getApplicationContext(),"error: "+statusCode,Toast.LENGTH_LONG).show();
                    // handle request errors yourself
                    ResponseBody errorBody = response.errorBody();
                    log("failed get users : "+errorBody);
                }
            }

            @Override
            public void onFailure(Call<PojoUser> call, Throwable t) {
                // handle execution failures like no internet connectivity
                log("Error while getting users");
            }
        });
    }

    private void log(String message) {
        Log.d(Const.DEBUG_TAG, getClass().getSimpleName() + " :" + message);

    }

}
