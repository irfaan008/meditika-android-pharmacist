package com.meditika.pharmacist.auth;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.meditika.pharmacist.R;
import com.meditika.pharmacist.util.api.APIHelper;
import com.meditika.pharmacist.util.api.IMeditikaAPIService;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Vishwa android on 01-Jun-16.
 */
public class Fragment1_Registration extends android.support.v4.app.Fragment  {

    @Bind(R.id.stateSpinner)
    Spinner stateSpinner;


    @Bind(R.id.citySpinner)
    Spinner citySpinner;
    List<BojoCity> allCategory = null;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_registrationpage1, container, false);

       initViews(view);

          getStateInfo();

        getCityInfo();
        return view;
    }

    private void initViews(View view) {

        citySpinner = (Spinner) view.findViewById(R.id.citySpinner);
        citySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                citySpinner.setSelected(true);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        stateSpinner = (Spinner) view.findViewById(R.id.stateSpinner);
    }


    private void getStateInfo() {


        IMeditikaAPIService service= APIHelper.getAppServiceMethod();

        Call<PojoState> call = service.stateList();


        //asynchronous call
        call.enqueue(new Callback<PojoState>() {
            @Override
            public void onResponse(Call<PojoState> call, Response<PojoState> response) {
                String statusCode = response.body().getCode();

                if (response.isSuccessful())
                {


                    String statusMessage = response.body().getStatus();
                    List<BojoState> pojoStates = response.body().getData();
                    bindDataToStateSpinner(pojoStates);
                }
                     else {
                        Toast.makeText(getActivity(), "Something Went wrong", Toast.LENGTH_LONG);
                    }
            }

            @Override
            public void onFailure(Call<PojoState> call, Throwable t) {
                Toast.makeText(getActivity(), t.toString(), Toast.LENGTH_LONG);


            }
        });
    }


    private void bindDataToStateSpinner(List<BojoState> pojoStates) {

        List<String> stateList = new ArrayList<String>();

        for (int i = 0; i < pojoStates.size(); i++) {
            stateList.add(pojoStates.get(i).getStateName());

        }
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity().getApplicationContext(), android.R.layout.simple_spinner_item, stateList);
        stateSpinner.setAdapter(dataAdapter);
        
    }


    private void getCityInfo() {


        IMeditikaAPIService service= APIHelper.getAppServiceMethod();

        Call<PojoCity> call = service.cityList();
        //asynchronous call
        call.enqueue(new Callback<PojoCity>() {
            @Override
            public void onResponse(Call<PojoCity> call, Response<PojoCity> response) {

                String statusCode = response.body().getCode();
                if (statusCode.equalsIgnoreCase("200")) {
                    String statusMessage = response.body().getStatus();
                    List<BojoCity> pojoStates = response.body().getData();
                    bindDataToCitySpinner(pojoStates);


                } else {
                    Toast.makeText(getActivity(), "Something Went wrong", Toast.LENGTH_LONG);
                }
            }

            @Override
            public void onFailure(Call<PojoCity> call, Throwable t) {
                Toast.makeText(getActivity(), t.toString(), Toast.LENGTH_LONG);

            }
        });
    }

    private void bindDataToCitySpinner(List<BojoCity> pojoStates) {

        List<String> stateList = new ArrayList<String>();

        for (int i = 0; i < pojoStates.size(); i++) {
            stateList.add(pojoStates.get(i).getStateName());

        }
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity().getApplicationContext(), android.R.layout.simple_spinner_item, stateList);
        citySpinner.setAdapter(dataAdapter);


    }
}
