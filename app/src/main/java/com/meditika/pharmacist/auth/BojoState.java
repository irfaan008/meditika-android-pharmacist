package com.meditika.pharmacist.auth;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by admin on 6/2/2016.
 */
public class BojoState {
    @SerializedName("state_id")
    protected String state_id;


    @SerializedName("state_name")
    protected String state_name;

    public String getStateId() {
        return state_id;
    }

    public void setStateId(String stateId) {
        this.state_id = stateId;
    }

    public void setStateName(String stateName) {
        this.state_name = stateName;
    }

    public String getStateName() {
        return state_name;
    }
}
