package com.meditika.pharmacist.order;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.EditText;
import android.widget.ImageButton;

import com.meditika.pharmacist.R;
import com.meditika.pharmacist.share.BaseActivity;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Vishwa android on 28-May-16.
 */
public class OrderHistory extends BaseActivity {

    @Bind(R.id.imgnext)
    ImageButton imgNext;
    @Bind(R.id.etAdress)
    EditText etAddress;
    @Bind(R.id.etUserName)
    EditText etUserName;
    @Bind(R.id.etmobileNumber)
    EditText etmobileNumber;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orderhistory);

        ButterKnife.bind(this);



    }

    @OnClick(R.id.imgnext)
    public void nextPage()
    {



        String address2=etUserName.getText().toString().trim();
        if(address2.length()<1) {
            etUserName.setError("Invalid username");
            return;
        }
        String address1=etmobileNumber.getText().toString().trim();
        if(address1.length()<1) {
            etmobileNumber.setError("Invalid mobile number");
            return;
        }
        String address=etAddress.getText().toString().trim();
        if(address.length()<1) {
            etAddress.setError("Invalid address");
            return;
        }
        startActivity(new Intent(OrderHistory.this,Discription.class));

    }

}
