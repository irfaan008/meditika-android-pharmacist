package com.meditika.pharmacist;

import android.content.Intent;
import android.os.Bundle;

import com.meditika.pharmacist.auth.LoginActivity;
import com.meditika.pharmacist.auth.RegistrationActivity;
import com.meditika.pharmacist.share.BaseActivity;
import com.meditika.pharmacist.share.MainActivity;

/**
 * Created by Irfan on 23/05/16.
 */
public class LauncherActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent=new Intent(this, RegistrationActivity.class);
        startActivity(intent);
        finish();
    }
}
