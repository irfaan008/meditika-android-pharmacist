package com.meditika.pharmacist;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.TextView;

import com.meditika.pharmacist.auth.PojoUser;
import com.meditika.pharmacist.share.BaseActivity;
import com.meditika.pharmacist.util.Const;
import com.meditika.pharmacist.util.api.APIHelper;
import com.meditika.pharmacist.util.api.IMeditikaAPIService;

import butterknife.Bind;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Irfan on 27/05/16.
 */
public class TestActivity extends BaseActivity {

    @Bind(R.id.tv1)
    TextView tv1;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_test);
        ButterKnife.bind(this);

        callTestMethod();
    }

    private void callTestMethod() {

        IMeditikaAPIService service= APIHelper.getAppServiceMethod();

        Call<PojoUser> call = service.getUsers();
        call.enqueue(new Callback<PojoUser>() {
            @Override
            public void onResponse(Call<PojoUser> call, Response<PojoUser> response) {
                // response.isSuccessful() is true if the response code is 2xx
                if (response.isSuccessful()) {
                    log("success get usres");
                    PojoUser user = response.body();
                    log("usres statu: "+user.getAllUser().get(0));

                } else {
                    int statusCode = response.code();

                    // handle request errors yourself
                    ResponseBody errorBody = response.errorBody();
                    log("failed get users : "+errorBody);
                }
            }

            @Override
            public void onFailure(Call<PojoUser> call, Throwable t) {
                // handle execution failures like no internet connectivity
                log("Error while getting users");
            }
        });
    }

    private void log(String message) {
        Log.d(Const.DEBUG_TAG, getClass().getSimpleName() + " :" + message);

    }
}
