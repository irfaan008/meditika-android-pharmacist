package com.meditika.pharmacist.util;

/**
 * Created by Irfan on 27/05/16.
 */
public class Const {
    public static final String KEY_USER_NAME = "keyUserName";
    public static final String KEY_STORE_NAME = "keyStoreName";
    public static final String KEY_PHARMACIST_NAME = "keyPharmacistName";
    public static final String KEY_DISPLAY_NAME = "keyDisplayName";
    public static final String KEY_AUTH_TOKEN = "keyAuthToken";
    public static final String KEY_ADDRESS = "keyAddress";
    public static String API_BASE_URL="https://meditika-api.herokuapp.com/api/";
    public static String DEBUG_TAG="MedApi";
    public static int CODE_SUCCESS=200;
    public static int CODE_FAILED=100;
    public static String KEY_ACCOUNT_ID="keyAccountId";
}
