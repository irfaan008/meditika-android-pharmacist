package com.meditika.pharmacist.util;

import android.content.Context;
import android.content.pm.PackageManager;

/**
 * Created by Irfan on 27/05/16.
 */
public class Common {

    public static String getVersionName(Context context) {
        String versionName = "";
        try {
            versionName = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return "Version: "+ versionName;
    }
}
