package com.meditika.pharmacist.util.api;


import android.content.Context;

import com.meditika.pharmacist.share.BaseActivity;
import com.meditika.pharmacist.util.Const;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.protobuf.ProtoConverterFactory;


/**
 * Created by Irfan on 22/09/15.
 */
public class APIHelper {

    BaseActivity activity;
    Context context;
    String url = Const.API_BASE_URL;

    IMeditikaAPIService service;


    public APIHelper(BaseActivity activity) {
        this.activity = activity;
        this.context = activity.getApplicationContext();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(ProtoConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();


        service = retrofit.create(IMeditikaAPIService.class);

    }

    public static IMeditikaAPIService getAppServiceMethod() {
        String url = Const.API_BASE_URL;

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit.create(IMeditikaAPIService.class);
    }


}
