package com.meditika.pharmacist.util.api;

import com.meditika.pharmacist.auth.PojoCity;
import com.meditika.pharmacist.auth.PojoState;
import com.meditika.pharmacist.auth.PojoUser;
import com.meditika.pharmacist.auth.BojoParticularCity;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by Irfan on 27/05/16.
 */
public interface IMeditikaAPIService {

    @FormUrlEncoded
    @POST(" login")
    Call<PojoUser> login(@Field("username") String username, @Field("password") String password);

    @GET("users")
    Call<PojoUser> getUsers();


    @GET("meta/states")
    Call<PojoState> stateList();

    @GET("meta/cities")
    Call<PojoCity> cityList();

    @GET("meta/cities/:cityId")
    Call<BojoParticularCity> particularcity();

}
